module.exports = {
  env: {
    browser: true,
    jquery: true,
  },
  globals: {
    'ajaxify': true,
    'app': true,
    'define': true,
    'socket': true,
  },
};
