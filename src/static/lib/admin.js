define('admin/plugins/quickstart', ['settings'], (Settings) => {
  const ACP = {};

  ACP.init = () => {
    Settings.load('quickstart', $('.quickstart-settings'));

    $('#save').on('click', () => {
      Settings.save('quickstart', $('.quickstart-settings'), () => {
        app.alert({
          type: 'success',
          alert_id: 'quickstart-saved',
          title: 'Settings Saved',
          message: 'Please reload your NodeBB to apply these settings',
          clickfn() {
            socket.emit('admin.reload');
          },
        });
      });
    });
  };

  return ACP;
});
