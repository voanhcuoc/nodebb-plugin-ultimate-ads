/* eslint no-underscore-dangle: "off" */

import initWrapper from './lib/init';

const _meta = module.parent.require('./meta');

export const init = initWrapper();

export const addAdminNavigation = (header, callback) => {
  header.plugins.push({
    route: '/plugins/quickstart',
    icon: 'fa-money',
    name: 'Ultimate Ads',
  });

  callback(null, header);
};

export const appendConfig = (config, callback) => {
  _meta.settings.get('ultimate-ads', (err, settings) => {
    if (err) {
      callback(null, config);
    }

    callback(null, { ...config, 'ultimate-ads': settings });
  });
};
