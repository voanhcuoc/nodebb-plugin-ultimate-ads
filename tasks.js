/* eslint import/no-extraneous-dependencies: "off" */

import Start from 'start';
import reporter from 'start-pretty-reporter';
import env from 'start-env';
import files from 'start-files';
import watch from 'start-watch';
import clean from 'start-clean';
import copy from 'start-copy';
import read from 'start-read';
import babel from 'start-babel';
import write from 'start-write';
import eslint from 'start-eslint';
import mocha from 'start-mocha';
import * as istanbul from 'start-istanbul';

export const start = Start(reporter());

export const build = () => start(
  env('NODE_ENV', 'production'),
  files('build/'),
  clean(),
  files('src/**/*.js'),
  read(),
  babel({ sourceMaps: true }),
  write('build/'),
  files('src/**/*.{json,tpl,less}'),
  copy('build/'),
);

export const dev = () => start(
  env('NODE_ENV', 'development'),
  files('build/'),
  clean(),
  files('src/**/*.js'),
  read(),
  babel({ sourceMaps: true }),
  write('build/'),
  files('src/**/*.{json,tpl,less}'),
  copy('build/'),
  watch('src/**/*.js')(file => start(
    // FIXME watch not working
    files(file),
    read(),
    babel({ sourceMaps: true }),
    write('build/'),
  )),
);

export const lint = () => start(
  env('NODE_ENV', 'test'),
  files(['src/**/*.js', 'test/**/*.js']),
  eslint(),
);

export const test = () => start(
  env('NODE_ENV', 'test'),
  files('test/entry.js'),
  mocha({ ui: 'tdd' }),
);

export const tdd = () => start(
  files(['src/**/*.js', 'test/**/*.js']),
  watch(test),
);

export const coverage = () => start(
  env('NODE_ENV', 'test'),
  files('coverage/'),
  clean(),
  files('src/**/*.js'),
  istanbul.instrument({ esModules: true }),
  test,
  istanbul.report(['lcovonly', 'html', 'text-summary']),
);

export const prepush = () => start(
  lint,
  coverage,
);
